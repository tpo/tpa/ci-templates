#!/bin/bash

# Exit on error, undefined variable, and errors in a pipeline
set -euo pipefail

# Retry limit for Git operations
RETRY_LIMIT=3
RETRY_DELAY=5  # in seconds

REPOSITORIES=(
    "https://gitlab.torproject.org/tpo/applications/tor-browser.git"
    "https://gitlab.torproject.org/tpo/applications/mullvad-browser.git"
)
MAIN_REPOSITORY="tor-browser"
CLONE_PATH="$HOME/.cache/apps-repos/tor-browser.git" # This folder needs to be mounted to the containers of Gitlab jobs.

retry() {
    local n=0
    until [ "$n" -ge "$RETRY_LIMIT" ]; do
        "$@" && break
        n=$((n+1))
        echo "Attempt $n failed! Retrying in $RETRY_DELAY seconds."
        sleep "$RETRY_DELAY"
    done

    if [ "$n" -ge "$RETRY_LIMIT" ]; then
        echo "Error: $1 failed after $RETRY_LIMIT attempts. Exiting."
        exit 1
    fi
}

if [ ! -d "$CLONE_PATH" ]; then
    echo "Repository not found in $CLONE_PATH. Initializing."
    mkdir -p "$CLONE_PATH"
    git -C "$CLONE_PATH" --bare init || {
        echo "Failed to initialize repository"
        exit 1
    }
    for REPOSITORY in "${REPOSITORIES[@]}"; do
        REPO_NAME=$(basename "$REPOSITORY" .git)
        git -C "$CLONE_PATH" remote add "$REPO_NAME" "$REPOSITORY" || {
            echo "Failed to add remote $REPOSITORY"
            exit 1
        }
    done
else
    echo "Repository found in $CLONE_PATH. Checking remotes."
    for REPOSITORY in "${REPOSITORIES[@]}"; do
        REPO_NAME=$(basename "$REPOSITORY" .git)
        if git -C "$CLONE_PATH" remote get-url "$REPO_NAME" &>/dev/null; then
            CURRENT_URL=$(git -C "$CLONE_PATH" remote get-url "$REPO_NAME")
            if [ "$CURRENT_URL" != "$REPOSITORY" ]; then
                echo "Remote $REPO_NAME URL is incorrect. Updating URL."
                git -C "$CLONE_PATH" remote set-url "$REPO_NAME" "$REPOSITORY" || {
                    echo "Failed to update remote URL for $REPO_NAME"
                    exit 1
                }
            else
                echo "Remote $REPO_NAME URL is correct."
            fi
        else
            echo "Remote $REPO_NAME not found. Adding remote."
            git -C "$CLONE_PATH" remote add "$REPO_NAME" "$REPOSITORY" || {
                echo "Failed to add remote $REPOSITORY"
                exit 1
            }
        fi
    done
fi

cd "$CLONE_PATH"

DEFAULT_BRANCH_NAME=$(git ls-remote --symref "$MAIN_REPOSITORY" HEAD | awk '/^ref:/ {gsub("refs/heads/", "", $2); print $2}') || {
    echo "Error: Failed to retrieve default branch name."
    exit 1
}

echo "Detected default branch: $DEFAULT_BRANCH_NAME"

echo "Fetching latest changes from $DEFAULT_BRANCH_NAME."
retry git fetch "$MAIN_REPOSITORY" "$DEFAULT_BRANCH_NAME"

echo "Updating references."
LATEST_COMMIT_SHA=$(git show --no-patch FETCH_HEAD --format=%H)
echo "$LATEST_COMMIT_SHA" > refs/heads/HEAD || {
    echo "Error: Failed to update reference for HEAD"
    exit 1
}
echo "$LATEST_COMMIT_SHA" > "refs/heads/$DEFAULT_BRANCH_NAME" || {
    echo "Error: Failed to update reference for $DEFAULT_BRANCH_NAME"
    exit 1
}

echo "Fetching latest changes from all branches and all repositories."
retry git fetch --all
